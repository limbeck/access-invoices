<?php

namespace App\DataFixtures;

use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use App\Entity\Invoice;
use App\Entity\User;
use App\DataFixtures\UserFixtures;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;

class InvoiceFixtures extends Fixture implements DependentFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        $user = $manager->getRepository(User::class)->findOneByEmail('user@access-sap.com');

        for ($i=1; $i <= 100; $i++) { 

        	$invoice = new Invoice();
        	$invoice->setDesignation("Désignation-$i");
        	$invoice->setDescription("Description de la facture $i");
        	$invoice->setPriceBeforeVat(rand(1000, 99999));
            $invoice->setUser($user);
        	$manager->persist($invoice);
        }

        $manager->flush();
    }

    public function getDependencies()
    {
        return [
            UserFixtures::class,
        ];
    }
}

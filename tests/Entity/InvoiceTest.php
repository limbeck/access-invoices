<?php 
namespace App\Tests\Entity;

use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use App\Entity\Invoice;
use App\DataFixtures\InvoiceFixtures;
use Symfony\Component\Validator\Validation;

class InvoiceTest extends KernelTestCase
{
	public function getInvoice(): Invoice
	{
		return (new Invoice())
			->setDesignation('Désignation de test')
			->setDescription('Description de test')
			->setPriceBeforeVat(1000);
	}

	public function assertHasErrors(Invoice $invoice, $number = 0)
	{
		$validator = Validation::createValidator();
		$errors = $validator->validate($invoice);

		$messages = [];
		foreach ($errors as $error) {
			$messages[] = $error->getPropertyPath() . ' => ' . $error->getMessage();
		}
		$this->assertCount($number, $errors, implode(', ', $messages));
	}

	public function testValidEntity()
	{
		$this->assertHasErrors($this->getInvoice(), 0);
	}

	public function testVatAmount()
	{
		$invoice = $this->getInvoice();
		$this->assertEquals($invoice->getVatAmount(), 200);
	}

	public function testPriceAfterVat()
	{
		$invoice = $this->getInvoice();
		$this->assertEquals($invoice->getPriceAfterVat(), 1200);
	}
}
<?php 
namespace App\Tests\Repository;

use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use App\Repository\InvoiceRepository;
use Liip\TestFixturesBundle\Test\FixturesTrait;
use App\DataFixtures\InvoiceFixtures;

class InvoiceRepositoryTest extends KernelTestCase
{
	use FixturesTrait;

	public function testCount()
	{
		$this->loadFixtures([InvoiceFixtures::class]);
		$invoices = self::$container->get(InvoiceRepository::class)->count([]);
		$this->assertEquals(100, $invoices);
	}
}
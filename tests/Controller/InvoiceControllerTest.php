<?php 
namespace App\Tests\Repository;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\HttpFoundation\Response;

class InvoiceControllerTest extends WebTestCase
{
	public function testHomePageIsOk()
	{
		$client = static::createClient();
		$client->request('GET', '/');
		$this->assertResponseStatusCodeSame(Response::HTTP_OK);
	}
}